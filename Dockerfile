# Base image
FROM ubuntu:18.04

# Chisel Dependencies

# Chisel is a Scala Library which depends on the installation of Java JDK 1.8 or a later version
RUN apt update
RUN apt install -y default-jdk

# Install the Scala build tool (sbt)
RUN echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | tee /etc/apt/sources.list.d/sbt.list
RUN echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | tee /etc/apt/sources.list.d/sbt_old.list
RUN apt install -y curl gnupg2
RUN curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | apt-key add
RUN apt-get update
RUN apt-get install -y sbt

# Git and Make installation
RUN apt install -y git make

# Install SSH-Server
RUN apt update && apt install -y openssh-server

# Create a user “chisel” and group “chiselgroup”
RUN groupadd chiselgroup && useradd -ms /bin/bash -g chiselgroup chisel

# Create chisel user directory in home
RUN mkdir -p /home/chisel/.ssh

# Clone the Matrix Multiplication Repository
RUN cd /home/chisel/ && git clone https://gitlab.com/julianmoruav/matrix-multiplication.git

# Change ownership of the matrix-multiplication directory, so that the sbt tool has the access permissions
RUN chown -R chisel:chiselgroup /home/chisel/matrix-multiplication/ && chmod -R 700 /home/chisel/matrix-multiplication/

# Copy the ssh public key in the authorized_keys file 
COPY id_rsa.pub /home/chisel/.ssh/authorized_keys

# Change ownership of the key file 
RUN chown chisel:chiselgroup /home/chisel/.ssh/authorized_keys && chmod 600 /home/chisel/.ssh/authorized_keys

# Start SSH service
RUN service ssh start

# Expose Docker port 22
EXPOSE 22

# Runs SSH Daemon
CMD ["/usr/sbin/sshd","-D"]
