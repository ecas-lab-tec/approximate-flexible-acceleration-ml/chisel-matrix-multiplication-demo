.PHONY: clean

# Default Parameters
M=2
N=2
P=2
W=8

# Files:
MM_SRC=src/main/scala/mm/mm.scala
MM_TEST=src/test/scala/mm/mm_tests.scala

# Useful names:
OUTDIR=generated
VCD_FILE=$(OUTDIR)/mmTest.MMTests*/MatrixMultiplication.vcd
VERILOG_FILE=$(OUTDIR)/MatrixMultiplication.v

all: tests

# Rule for generating the verilog file
$(VERILOG_FILE): $(MM_SRC)
	sbt "runMain matrixmultiplications.MatrixMultiplication $M $N $P $W"

verilog: $(VERILOG_FILE)

# Rule for running all the tests
$(VCD_FILE): $(MM_SRC) $(MM_TEST)
	sbt "testOnly matrixmultiplications.MatrixMultiplicationTests"

tests: $(VCD_FILE)

# Rule to clean the workspace
clean: 
	@rm -rf generated
