// See README.md for license details.

ThisBuild / scalaVersion     := "2.12.13"
ThisBuild / version          := "0.1.0"
ThisBuild / organization     := "com.github.julianmoruav"
lazy val root = (project in file("."))
  .settings(
    name := "matrix-multiplication",
    libraryDependencies ++= Seq(
      "edu.berkeley.cs" %% "chisel3" % "3.2.+",
      "edu.berkeley.cs" %% "chisel-iotesters" % "1.4.+",
      "org.scalatest" %% "scalatest" % "3.0.5" % Test,
      "org.scalanlp" %% "breeze" % "2.0.1-RC1",
      "org.scalanlp" %% "breeze-viz" % "2.0.1-RC1"
    ),
    scalacOptions ++= Seq(
      "-Xsource:2.11",
      "-language:reflectiveCalls",
      "-deprecation",
      "-feature"
    )
  )
