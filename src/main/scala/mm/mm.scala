/*========================================================================
   Copyright 2022 Julián Morúa Vindas

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
=========================================================================*/

package matrixmultiplications

import chisel3._
import chisel3.util._

/* 
 * A module for matrix mutliplication:
 *      Matrix1 one has dimensions mxn.
 *      Matrix2 has dimensions nxp.
 *      Wires have width w.
 *      Output matrix has dimensions mxp and width (2*w + log2Floor(n))
 */
class MatrixMultiplication (m: Int, n: Int, p: Int, w: Int) extends Module {
    // Interface:
    val io = IO(new Bundle {
        val mat1 = Input(Vec(m, Vec(n, UInt(w.W))))
        val mat2 = Input(Vec(n, Vec(p, UInt(w.W))))
        val out = Output(Vec(m, Vec(p, UInt((2*w + log2Floor(n)).W))))
    })

    // Transpose mat2 to simplify access to column vectors in the first dimension of the matrix:
    var transposed_mat2 = Wire(Vec(p, Vec(n, UInt(w.W))))
    for (row <- 0 until n) {
        for (col <- 0 until p) {
            transposed_mat2(col)(row) := io.mat2(row)(col)
        }
    }

    // Compute the dotproduct of the corresponding row and column vectors:
    for (row <- 0 until m){
        for (col <- 0 until p) {
            io.out(row)(col) := dotproduct(io.mat1(row), transposed_mat2(col))
        }
    }

    // Lightweight component to compute the dot product between two vectors:
    def dotproduct(vec1: Vec[UInt], vec2: Vec[UInt]) = {
        require(vec1.size == vec2.size)
        val res = vec1.zip(vec2).map { case (a, b) => a * b }.reduce(_ +& _) // +& operand expands width if needed.
        res // return
    }
}

/*
 * Main program to generate the Verilog code.
 * The matrix dimensions and bit widths are passed as CLI arguments.
 */
object MatrixMultiplication extends App {
    // Get the args:
    val m = args(0).toInt
    val n = args(1).toInt
    val p = args(2).toInt
    val w = args(3).toInt

    // Generate verilog:
    val log = s"""Generating the MatrixMultiplication Verilog with the following parameters:
                | * m=$m
                | * n=$n
                | * p=$p
                | * w=$w""".stripMargin
    println(log)
    chisel3.Driver.execute(Array("--target-dir", "generated"), () => new MatrixMultiplication(m, n, p, w))
}
